package kmodule

{
	local:  true
	domain: "kumori.systems"
	module: "builtins/inbound"
	cue:    "v0.4.3"
	version: [
		1,
		3,
		1,
	]
	dependencies: "kumori.systems/kumori": {
		target: "kumori.systems/kumori/@1.1.6"
		query:  "1.1.6"
	}
	sums: "kumori.systems/kumori/@1.1.6": "jsXEYdYtlen2UgwDYbUCGWULqQIigC6HmkexXkyp/Mo="
	spec: [
		1,
		0,
	]
}
