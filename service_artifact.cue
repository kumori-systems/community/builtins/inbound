package service

import (
  k  "kumori.systems/kumori:kumori"
)

// Definition of a list of IPs (regular IPs or CIDRs, defined via regular expression)
let ip = "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
let cidr = "\(ip)/([0-9]|[1-2][0-9]|3[0-2])"
#ipcidr: =~ "^\(ip)|\(cidr)$"
#IPList: [...(#ipcidr)]

#Artifact: {

  ref: name: "inbound"

  description: {
     builtin: true
     config: {
      parameter: {
        type: *"https" | "tcp"
        if type == "https" {
          clientcert: bool | *false
          if clientcert == true {
            certrequired: bool | *false
          }
          websocket: bool | *false
          // Name of the header (for example, "X-Real-IP") where the remote address
          // will be setted. This may not be the physical remote address of the
          // peer if the address has been inferred from the x-forwarded-for
          // (depends on cluster configuration)
          remoteaddressheader: string | *""
          // Clean the x-forwarded-for header
          cleanxforwardedfor: bool | * false
          // An http inbound  can declare a list of allowed or denied IPs
          //
          // This syntax is preferable, because it prevents the simultaneous
          // existence of "allowediplist" and "deniediplist", but an error occurs
          // during the service spread.
          //   accesspolicies?: { allowediplist: #IPList } | { deniediplist: #IPList }
          //
          // (currently not supported for tpc inbounds)
          //
          accesspolicies?: {
            allowediplist?: #IPList
            deniediplist?: #IPList
          }
        }
      }
      resource: {
        if config.parameter.type == "https" {
          servercert   : k.#Certificate
          serverdomain : k.#Domain
          clientcertca?: k.#CA
        }
        if config.parameter.type == "tcp" {
          port: k.#Port
        }
      }
    }
    srv: client: inbound: _
  }
}
