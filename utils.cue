package utils

#Ingress: {
	subpath: {
		prefix:       *"/.*" | string
		prefix_regex: *true | bool
		rewrite:      #Rewrite
	}
	certrequired: *false | bool
}

#Rewrite: {
	substitution:  *"" | string
	pattern:       *"" | string
	rewrite_regex: *false | bool
}

#InboundMetadata: {
	"__ingress": #Ingress
  ...
}
